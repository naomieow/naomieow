# Hi, I'm Mia 👋
I'm a transgender developer from the UK who has been coding since I got my hands on a computer!  
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Y8Y5LFJLX)

## Links 🌍
- [🌐 Personal Site](https://www.naomieow.xyz/)
- [💬 Discord](https://discord.naomieow.xyz/)
- [📘 poly.me](https://poly.naomieow.xyz/)

## Languages 🖥️
### Proficient:
- 🐍 Python
- ☕ Java
- 📜 JavaScript/TypeScript
### Learning:
- 🦀 Rust
- 💎 Ruby

## Notable Projects
### [Mystcraft: Ages](https://gitlab.com/mystcraft-ages/mystcraft-ages)
[![Latest Release](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/badges/release.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/releases) 
[![pipeline status](https://gitlab.com/mystcraft-ages/mystcraft-ages/badges/1.20.1/pipeline.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/commits/1.20.1) 
[![coverage report](https://gitlab.com/mystcraft-ages/mystcraft-ages/badges/1.20.1/coverage.svg)](https://gitlab.com/mystcraft-ages/mystcraft-ages/-/commits/1.20.1)    

Mystcraft: Ages is a modern rewrite and reimagining of the original [Mystcraft](https://www.curseforge.com/minecraft/mc-mods/mystcraft) mod for Minecraft  

### [Chlorine](https://gitlab.com/chlorine-lang/chlorine-rs) 

[![Latest Release](https://gitlab.com/chlorine-lang/chlorine-rs/-/badges/release.svg)](https://gitlab.com/chlorine-lang/chlorine-rs/-/releases) 
[![pipeline status](https://gitlab.com/chlorine-lang/chlorine-rs/badges/main/pipeline.svg)]()
[![coverage report](https://gitlab.com/chlorine-lang/chlorine-rs/badges/main/coverage.svg)](https://gitlab.com/chlorine-lang/chlorine-rs/-/commits/main) 

Chlorine is a compiled hobby-language written from the ground up in Rust.  
